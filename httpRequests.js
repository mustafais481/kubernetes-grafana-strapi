require('dotenv').config();
const axios = require('axios');

const apiUrl = process.env.API_ENDPOINT_RESTAURANT;
const authToken = process.env.STRAPI_AUTH_TOKEN;

let successfulCalls = 0;
let failedCalls = 0;
let totalExecutionTime = 0;

async function createRestaurant(data) {
  try {
    const start = performance.now(); // Enregistre le temps de début
    const response = await axios.post(apiUrl, data, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    });
    const end = performance.now(); // Enregistre le temps de fin
    console.log('Opération POST réussie:', response.data);
    successfulCalls++;
    const executionTime = end - start;
    totalExecutionTime += executionTime;
    console.log('Temps d\'exécution:', executionTime, 'ms'); // Affiche le temps d'exécution
  } catch (error) {
    console.error('Erreur lors de l\'opération POST :', error.message);
    failedCalls++;
  }
}

async function createRestaurantsParallel() {
  const promises = [];

  const scriptStart = performance.now(); // Enregistre le temps de début du script

  for (let i = 0; i < 100000; i++) {
    const restaurantData = {
      data: {
        title: `Nouveau Restaurant ${i}`,
        description: [
          {
            "type": "paragraph",
            "children": [
              {
                "text": "menus déjeuner et vins mexicains dans un restaurant " + i,
                "type": "text"
              }
            ]
          }
        ]
      },
    };
    promises.push(createRestaurant(restaurantData));
  }

  await Promise.all(promises);

  const scriptEnd = performance.now(); // Enregistre le temps de fin du script

  console.log('Nombre d\'appels réussis:', successfulCalls);
  console.log('Nombre d\'appels échoués:', failedCalls);
  console.log('Temps total d\'exécution du script:', scriptEnd - scriptStart, 'ms');
  console.log('Temps moyen d\'exécution par appel:', totalExecutionTime / (successfulCalls + failedCalls), 'ms');
}

// createRestaurantsParallel();
async function getRestaurant() {
  try {
    const start = performance.now();
    const response = await axios.get(apiUrl, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    });
    const end = performance.now();
    console.log('Opération GET réussie:', response.data);
    successfulCalls++;
    const executionTime = end - start;
    totalExecutionTime += executionTime;
    console.log('Temps d\'exécution:', executionTime, 'ms');
  } catch (error) {
    console.error('Erreur lors de l\'opération GET :', error.message);
    failedCalls++;
  }
}

async function runParallelRequests(numRequests) {
  const start = performance.now();
  const requests = Array.from({ length: numRequests }, () => getRestaurant());

  try {
    await Promise.all(requests);
  } catch (error) {
    console.error('Une ou plusieurs requêtes ont échoué :', error.message);
  }

  const end = performance.now();
  console.log('Nombre total d\'appels réussis:', successfulCalls);
  console.log('Nombre total d\'appels échoués:', failedCalls);
  console.log('Temps total d\'exécution pour les', numRequests, 'appels en parallèle:', end - start, 'ms');
}

async function runInfiniteParallelRequests() {
  while (true) {
    await Promise.all([
      runParallelRequests(1000)   // ... ajoutez autant de requêtes que nécessaire
    ]);
  }
}

// Appel de la méthode runInfiniteParallelRequests
runInfiniteParallelRequests();
// Exemple : Lancer 1000 appels en parallèle
// runParallelRequests(1000);