FROM node:18-alpine
RUN apk update && apk add --no-cache build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev git
ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

WORKDIR /opt/
COPY package.json ./
RUN npm install -g node-gyp
RUN npm install -g @esbuild/linux-x64
RUN npm config set fetch-retry-maxtimeout 600000 -g
RUN npm cache clean --force
RUN npm install


WORKDIR /opt/app
COPY . .

RUN ["npm", "run", "build"]
EXPOSE 1337
CMD ["npm", "run", "develop"]
