// config/plugins.js
const { apolloPrometheusPlugin } = require('strapi-prometheus');

module.exports = [
  {
    'strapi-prometheus': {
      enabled: true,
    },
  },
  {
    graphql: {
      enabled: true,
      config: {
        apolloServer: {
          plugins: [apolloPrometheusPlugin], 
          tracing: true, 
        },
      },
    },
  },
];
